const inherits = require('util').inherits
const Component = require('react').Component
const h = require('react-hyperscript')
const connect = require('react-redux').connect
const Eth = require('ethjs');

module.exports = connect(mapStateToProps)(AppRoot)

function mapStateToProps (state) {
  return state
}

inherits(AppRoot, Component)
function AppRoot () {
  Component.call(this)
}

AppRoot.prototype.render = function () {
  const props = this.props
  const { eth, loading, nonce, error, web3Found } = props
  console.dir(props)

  return (
    h('.content', {
      style: {
        color: 'grey',
        padding: '15px',
      },
    }, [

      h('h1', `Too deep into ICO risks? Easy conversion to Sai Stablecoin using 0x`),
      h('h1', `E-commerce but don't want the volatility? Easily accept ERC20 Tokens and receive Sai Stablecoin`),

      !web3Found ?

        h('div', [
          h('You should get MetaMask for the full experience!'),

          h(MetaMaskLink, { style: { width: '250px' } }),
        ])
        : loading ? h('span', 'Loading...') : h('button', {
          onClick: () => this.sendSai(),
        }, 'Send ETH and receive SAI stablecoin'),

      h('br'),
      nonce > 0 ? h('h2', `Cool!`) : null,
      h('br'),
      error ? h('span', { style: { color: '#212121' } }, error) : null,

    ])
  )
}
